# Introduction
This repository provides the tools necessary to deploy your own set of Tezos nodes using Helm and Kubernetes.

This deployment is a fork of the [tezos-k8s](https://github.com/oxheadalpha/tezos-k8s) projetct.

The Helm charts are stored in the [templates](templates/) folder, and two values files are provided: one for the testnet, one for the mainnet.

# Pre-requisites
* Install [kubectl](https://kubernetes.io/docs/tasks/tools/)
* Install [Helm](https://helm.sh/docs/intro/install/)
* Install [Terraform](https://learn.hashicorp.com/tutorials/terraform/install-cli)
* A Kubernetes Cluster with Helm
  * For AWS EKS users you can get the kubeconfig with the following command:
> https://docs.aws.amazon.com/eks/latest/userguide/create-kubeconfig.html
```
aws eks --region eu-west-1 update-kubeconfig --name complete-ncMfxemd
```

# Configuration
You can modify the configuration of your deployment by tweaking several parameters. For more information regarding the configuration, please refer to [`tezos-k8s`](https://github.com/oxheadalpha/tezos-k8s) configuration.

## Hardware resources
To modify the resources available to your nodes, you can modify the following lines in the `values` files:
```yaml
resources:
  requests:
    cpu: "3.0"
    memory: 12Gi
  limits:
    cpu: "3.0"
    memory: 12Gi
```
## Node configuration
To modify the configuration for the nodes, you will need to modify this section:
```yaml
nodes:
  tezos-node:
    storage_size: 512Gi
    runs:
      - octez_node
      - metrics
    instances:
      - is_bootstrap_node: true
        config:
          shell:
            history_mode: rolling
      - is_bootstrap_node: true
        config:
          shell:
            history_mode: rolling
```
To modify the number of nodes, you need to add/remove the following entry in the `instances` section:
```yaml
      - is_bootstrap_node: true
        config:
          shell:
            history_mode: rolling
```
Each one of this entry will be a specific node.


## Snapshot URLs
The snapshot URLs are defined with the following values:
```yaml
full_snapshot_url: https://mainnet.xtz-shots.io/full
rolling_snapshot_url: https://mainnet.xtz-shots.io/rolling
```

## Chain name
The chain you will be using is defined here
```yaml
node_config_network:
  chain_name: mainnet
```
Using a specific chain name will result in creating a side chain.

# Deploying the nodes
To deploy the nodes, you simply need to run the following command from the repository root
```bash
helm install <release-name> tezos-nodes/ -n <namespace> --values <values-file>
```
> Example:
> ```bash
> helm install mainnet-nodes tezos-nodes/ -n mainnet > --values tezos-nodes/values-mainnet.yaml
> ```

# Upgrade the nodes
To upgrade the nodes, you simply need to run the following command from the repository root
```bash
helm upgrade <release-name> tezos-nodes/ -n <namespace> --values <values-file>
```
> Example:
> ```bash
> helm upgrade mainnet-nodes tezos-nodes/ -n mainnet > --values tezos-nodes/values-mainnet.yaml
> ```

# Setting up monitoring
## Installing kube dashboard (AWS EKS)
> https://docs.aws.amazon.com/eks/latest/userguide/prometheus.html
```bash
kubectl apply -f https://raw.githubusercontent.com/kubernetes/dashboard/v2.0.5/aio/deploy/recommended.yaml
```

## Installing K8S Metrics Server and Prometheus/Grafana stack
```bash
kubectl apply -f https://github.com/kubernetes-sigs/metrics-server/releases/latest/download/components.yaml

helm repo add prometheus-community https://prometheus-community.github.io/helm-charts

helm install -n monitoring monitoring prometheus-community/kube-prometheus-stack --set prometheus.prometheusSpec.podMonitorSelectorNilUsesHelmValues=false --set prometheus.prometheusSpec.serviceMonitorSelectorNilUsesHelmValues=false
```

## Import the Tezos dashboard
From the Grafana dashboard, import the Tezos dashboard located [here](../monitoring/grafana/dashboards/tezos.json)
