LOCAL_URL="http://127.0.0.1:8732"
res="ok"
levels=""
while true; do {
  for url in {{ .Values.chainsLevel }}; do
    CHAIN_HEAD="$(curl -s -m 20 ${url}/chains/main/blocks/head/header | jq .level)"
    LOCAL_HEAD="$(curl -s -m 20 ${LOCAL_URL}/chains/main/blocks/head/header | jq .level)"

    if [[ ${CHAIN_HEAD} -gt "${LOCAL_HEAD} +4" ]]; then
      res="error"
      levels="$levels\n[KO] ${url}-${CHAIN_HEAD}"
    else
      res="ok"
      levels="$levels\n[OK] ${url}-${CHAIN_HEAD}"
    fi
  done

  if [ "$res" == "ok" ]; then
    printf "HTTP/1.1 200 OK\r\n\r\n[$(date)] Node is live $levels"
  else
    printf 'HTTP/1.1 500 Internal Server Error\r\n'
  fi
} | nc -l -p 8080; done