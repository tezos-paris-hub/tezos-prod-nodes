# install monitoring stack
```bash
helm install -n monitoring monitoring prometheus-community/kube-prometheus-stack --set prometheus.prometheusSpec.podMonitorSelectorNilUsesHelmValues=false --set prometheus.prometheusSpec.serviceMonitorSelectorNilUsesHelmValues=false
# Publicly expose the grafana dashboard
kubectl expose pod monitoring-grafana-6b49d6974f-9bs6x --port=3000 --type=LoadBalancer --name=grafana -n monitoring
```

# install netdata in cluster (+ add service monitor)
```
helm repo add netdata https://netdata.github.io/helmchart/
helm install netdata netdata/netdata -n monitoring
kubectl apply -f monitoring/netdata/service-monitor.yaml -n monitoring
```

# install cadvisor in cluster for prometheus (+ add service monitor)
```
helm repo add ckotzbauer https://ckotzbauer.github.io/helm-charts
helm install my-cadvisor ckotzbauer/cadvisor --version 1.4.0 -n monitoring
kubectl apply -f monitoring/cadvisor/service-monitor.yaml -n monitoring
```