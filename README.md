# Pre-requisites
* install kubectl
* install helm
* install terraform
* set up AWS account to have EKS permissions
* set up AWS CLI

# Deploy on AWS with Terraform
```
git clone https://github.com/terraform-aws-modules/terraform-aws-eks.git
cd /examples/complete
adapt file: examples/complete/main.tf to your needs
terraform init
terraform plan
terraform apply
```

# Get kubeconfig
```
# https://docs.aws.amazon.com/eks/latest/userguide/create-kubeconfig.html
aws eks --region eu-west-1 update-kubeconfig --name complete-ncMfxemd
````

# install kube dashboard
```
# https://docs.aws.amazon.com/eks/latest/userguide/prometheus.html
kubectl apply -f https://raw.githubusercontent.com/kubernetes/dashboard/v2.0.5/aio/deploy/recommended.yaml
```
# install metrics server and prometheus
```
kubectl apply -f https://github.com/kubernetes-sigs/metrics-server/releases/latest/download/components.yaml
kubectl create namespace prometheus
helm repo add prometheus-community https://prometheus-community.github.io/helm-charts
helm upgrade -i prometheus prometheus-community/prometheus \
    --namespace prometheus \
    --set alertmanager.persistentVolume.storageClass="gp2",server.persistentVolume.storageClass="gp2"
```

# install grafana stack
```
https://www.eksworkshop.com/intermediate/240_monitoring/deploy-grafana/
```
