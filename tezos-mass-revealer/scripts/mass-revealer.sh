#!/bin/sh
export TEZOS_CLIENT_UNSAFE_DISABLE_DISCLAIMER=yes

echo "█████████████████████████████████████████████████████████████████████████████████████████"
echo "██  ████████╗███████╗███████╗ ██████╗ ███████╗    ███╗   ███╗ █████╗ ███████╗███████╗  ██"
echo "██  ╚══██╔══╝██╔════╝╚══███╔╝██╔═══██╗██╔════╝    ████╗ ████║██╔══██╗██╔════╝██╔════╝  ██"
echo "██     ██║   █████╗    ███╔╝ ██║   ██║███████╗    ██╔████╔██║███████║███████╗███████╗  ██"
echo "██     ██║   ██╔══╝   ███╔╝  ██║   ██║╚════██║    ██║╚██╔╝██║██╔══██║╚════██║╚════██║  ██"
echo "██     ██║   ███████╗███████╗╚██████╔╝███████║    ██║ ╚═╝ ██║██║  ██║███████║███████║  ██"
echo "██     ╚═╝   ╚══════╝╚══════╝ ╚═════╝ ╚══════╝    ╚═╝     ╚═╝╚═╝  ╚═╝╚══════╝╚══════╝  ██"
echo "██  ██████╗ ███████╗██╗   ██╗███████╗ █████╗ ██╗     ███████╗██████╗                   ██"
echo "██  ██╔══██╗██╔════╝██║   ██║██╔════╝██╔══██╗██║     ██╔════╝██╔══██╗                  ██"
echo "██  ██████╔╝█████╗  ██║   ██║█████╗  ███████║██║     █████╗  ██████╔╝                  ██"
echo "██  ██╔══██╗██╔══╝  ╚██╗ ██╔╝██╔══╝  ██╔══██║██║     ██╔══╝  ██╔══██╗                  ██"
echo "██  ██║  ██║███████╗ ╚████╔╝ ███████╗██║  ██║███████╗███████╗██║  ██║                  ██"
echo "██  ╚═╝  ╚═╝╚══════╝  ╚═══╝  ╚══════╝╚═╝  ╚═╝╚══════╝╚══════╝╚═╝  ╚═╝                  ██"
echo "█████████████████████████████████████████████████████████████████████████████████████████"

echo
echo


total=$TOTAL
batch_size=$BATCH_SIZE
start=$START
i=$start
j=0

TEZ_VAR=/var/tezos/
TEZ_BIN=/usr/local/bin
CLIENT_DIR="$TEZ_VAR/client"
NODE_DIR="$TEZ_VAR/node"
NODE_DATA_DIR="$TEZ_VAR/node/data"

CLIENT="$TEZ_BIN/tezos-client -E http://104.196.8.169:8732 -w none -d $CLIENT_DIR"


mkdir -p $CLIENT_DIR

if [ ! -f $CLIENT_DIR/public_key_hashs ] && [ ! -f $CLIENT_DIR/public_keys ] && [ ! -f $CLIENT_DIR/secret_keys ]
then
    echo "Wallet data not present, downloading it"
    wget  -O $CLIENT_DIR/public_key_hashs "https://gitlab.com/tezos-paris-hub/tezos-infra/-/raw/main/load-testing/wallet/raw/public_key_hashs"
    wget  -O $CLIENT_DIR/public_keys "https://gitlab.com/tezos-paris-hub/tezos-infra/-/raw/main/load-testing/wallet/raw/public_keys"
    wget  -O $CLIENT_DIR/secret_keys "https://gitlab.com/tezos-paris-hub/tezos-infra/-/raw/main/load-testing/wallet/raw/secret_keys"
fi

args=""
while [ $i -le $total ]
do
    for j in `seq $j $batch_size`
    do
        if [ $i -le $total ]
        then
            args="${args} hang$i"
            i=`expr $i + 1`
        fi
    done
    $CLIENT reveal key for $args
    j=0
    args=""
    echo "Revealed $i public keys"
done
