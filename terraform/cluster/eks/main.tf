provider "aws" {
  region = "eu-west-1"
}

locals {
  name            = "prod-${random_string.suffix.result}"
  cluster_version = "1.21"
  region          = "eu-west-1"
  zone = "eu-west-1a"
}

################################################################################
# EKS Module
################################################################################

module "eks" {
  source = "../.."

  cluster_name    = local.name
  cluster_version = local.cluster_version

  vpc_id  = module.vpc.vpc_id
  subnets = module.vpc.private_subnets

  cluster_endpoint_private_access = true
  cluster_endpoint_public_access  = true

  node_groups_defaults = {
    ami_type  = "AL2_x86_64"
    # Required:
    # - 6 x 300 for Mainnet full nodes
    disk_size = 700
  }

  node_groups = {

    prod-1 = {
      disk_size = 700
      desired_capacity = 6
      max_capacity     = 10
      min_capacity     = 6

      # eq to:
      # 40 vCPUS
      # 160 Gb Memory
      # For following infra
      # - 6x Mainnet node: 3 vCPU 12 Gb Memory
      instance_types = ["t3a.xlarge"]
      capacity_type  = "ON_DEMAND"
      k8s_labels = {
        Example    = "managed_node_groups"
        GithubRepo = "terraform-aws-eks"
        GithubOrg  = "terraform-aws-modules"
      }
      additional_tags = {
        ExtraTag = "tezos-nodes"
      }
      taints = [
      ]
      update_config = {
        max_unavailable_percentage = 50 # or set `max_unavailable`
      }
    }
    prod-2 = {
      disk_size = 700
      desired_capacity = 2
      max_capacity     = 2
      min_capacity     = 2

      # eq to:
      # 8 vCPUS
      # 32 Gb Memory
      # For following infra
      # - Testnet node
      # - Rarible Prod
      instance_types = ["t3a.xlarge"]
      capacity_type  = "ON_DEMAND"
      k8s_labels = {
        Example    = "managed_node_groups"
        GithubRepo = "terraform-aws-eks"
        GithubOrg  = "terraform-aws-modules"
      }
      additional_tags = {
        ExtraTag = "tezos-nodes-testnet-monitoring"
      }
      taints = [
      ]
      update_config = {
        max_unavailable_percentage = 50 # or set `max_unavailable`
      }
    }

    prod-3 = {
      disk_size = 700
      desired_capacity = 1
      max_capacity     = 1
      min_capacity     = 1

      # eq to:
      # 16 vCPUS
      # 64 Gb Memory
      # For following infra
      # - Testnet node
      # - Rarible Prod
      instance_types = ["t3a.2xlarge"]
      capacity_type  = "ON_DEMAND"
      k8s_labels = {
        Example    = "managed_node_groups"
        GithubRepo = "terraform-aws-eks"
        GithubOrg  = "terraform-aws-modules"
      }
      additional_tags = {
        ExtraTag = "tezos-rarible"
      }
      taints = [
      ]
      update_config = {
        max_unavailable_percentage = 50 # or set `max_unavailable`
      }
    }
  }

  map_roles    = var.map_roles
  map_users    = var.map_users
  map_accounts = var.map_accounts

  tags = {
    Example    = local.name
    GithubRepo = "terraform-aws-eks"
    GithubOrg  = "terraform-aws-modules"
  }
}

################################################################################
# Kubernetes provider configuration
################################################################################

data "aws_eks_cluster" "cluster" {
  name = module.eks.cluster_id
}

data "aws_eks_cluster_auth" "cluster" {
  name = module.eks.cluster_id
}

provider "kubernetes" {
  host                   = data.aws_eks_cluster.cluster.endpoint
  cluster_ca_certificate = base64decode(data.aws_eks_cluster.cluster.certificate_authority[0].data)
  token                  = data.aws_eks_cluster_auth.cluster.token
}

################################################################################
# Supporting Resources
################################################################################

data "aws_availability_zones" "available" {
}

resource "random_string" "suffix" {
  length  = 8
  special = false
}

module "vpc" {
  source  = "terraform-aws-modules/vpc/aws"
  version = "~> 3.0"

  name                 = local.name
  cidr                 = "10.0.0.0/16"
  azs                  = data.aws_availability_zones.available.names
  private_subnets      = ["10.0.1.0/24", "10.0.2.0/24", "10.0.3.0/24"]
  public_subnets       = ["10.0.4.0/24", "10.0.5.0/24", "10.0.6.0/24"]
  enable_nat_gateway   = true
  single_nat_gateway   = true
  enable_dns_hostnames = true

  public_subnet_tags = {
    "kubernetes.io/cluster/${local.name}" = "shared"
    "kubernetes.io/role/elb"              = "1"
  }

  private_subnet_tags = {
    "kubernetes.io/cluster/${local.name}" = "shared"
    "kubernetes.io/role/internal-elb"     = "1"
  }

  tags = {
    Example    = local.name
    GithubRepo = "terraform-aws-eks"
    GithubOrg  = "terraform-aws-modules"
  }
}
