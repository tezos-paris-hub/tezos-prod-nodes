#!/bin/sh
export TEZOS_CLIENT_UNSAFE_DISABLE_DISCLAIMER=yes

echo "███████████████████████████████████████████████████████████████████████████████████████████████████████████████"
echo "██ ████████╗███████╗███████╗ ██████╗ ███████╗    ██████╗  █████╗ ██╗      █████╗ ███╗   ██╗ ██████╗███████╗  ██"
echo "██ ╚══██╔══╝██╔════╝╚══███╔╝██╔═══██╗██╔════╝    ██╔══██╗██╔══██╗██║     ██╔══██╗████╗  ██║██╔════╝██╔════╝  ██"
echo "██    ██║   █████╗    ███╔╝ ██║   ██║███████╗    ██████╔╝███████║██║     ███████║██╔██╗ ██║██║     █████╗    ██"
echo "██    ██║   ██╔══╝   ███╔╝  ██║   ██║╚════██║    ██╔══██╗██╔══██║██║     ██╔══██║██║╚██╗██║██║     ██╔══╝    ██"
echo "██    ██║   ███████╗███████╗╚██████╔╝███████║    ██████╔╝██║  ██║███████╗██║  ██║██║ ╚████║╚██████╗███████╗  ██"
echo "██  ███╗═╝ ███╗═█████╗═███╗═╝ ██╗═█████╗══██████╗ ███████╗██████╗╝╚══════╝╚═╝  ╚═╝╚═╝  ╚═══╝ ╚═════╝╚══════╝ ██"
echo "██  ████╗ ████║██╔══██╗████╗  ██║██╔══██╗██╔════╝ ██╔════╝██╔══██╗                                           ██"
echo "██  ██╔████╔██║███████║██╔██╗ ██║███████║██║  ███╗█████╗  ██████╔╝                                           ██"
echo "██  ██║╚██╔╝██║██╔══██║██║╚██╗██║██╔══██║██║   ██║██╔══╝  ██╔══██╗                                           ██"
echo "██  ██║ ╚═╝ ██║██║  ██║██║ ╚████║██║  ██║╚██████╔╝███████╗██║  ██║                                           ██"
echo "██  ╚═╝     ╚═╝╚═╝  ╚═╝╚═╝  ╚═══╝╚═╝  ╚═╝ ╚═════╝ ╚══════╝╚═╝  ╚═╝                                           ██"
echo "███████████████████████████████████████████████████████████████████████████████████████████████████████████████"

echo
echo

input={{ .Values.filePath }}
amount="$AMOUNT"
pk="$SECRET_KEY"
sleep=$SLEEP
threshold=$THRESHOLD

if [ ! $input ]
then
    echo "MISSING OPTIONS (WALLET LIST)"
    echo "A wallet list is mandatory. If you don't have any you can use the get-wallet-script helper to extract it" \
    "from the tezos-client config folder."
    echo "It must have one wallet adress per line, without quote/double quote"
    echo
    exit
fi

if [ ! $amount ]
then
    echo "MISSING OPTIONS (AMOUNT)"
    echo "The topup amount is mandatory."
    echo
    exit
fi

if [ ! $threshold ]
then
    echo "MISSING OPTIONS (THRESHOLD)"
    echo "The threshold amount is mandatory."
    echo
    exit
fi

if [ ! $pk ]
then
    echo "MISSING OPTIONS (SECRET_KEY)"
    echo "SECRET_KEY env var must be set. Format must be <protocol:secret_key>." \
    "\nExample: unencrypted:edsk3gUfUPyBSfrS9CCpmdiQsTCHGkviBDusMxDJstFtojtc1zcpsh"
    echo
    exit
fi

if [ ! $sleep ]
then
    sleep=0
fi

TEZ_BIN=/usr/local/bin
TEZ_VAR=/var/tezos/client

mkdir -p $TEZ_VAR

if [ ! -f $TEZ_VAR/jq ]
then
echo "jq not present, downloading it"
wget  -O $TEZ_VAR/jq "https://github.com/stedolan/jq/releases/download/jq-1.6/jq-linux64"
chmod +x $TEZ_VAR/jq
fi

echo http://tezos-node.{{ .Release.Namespace }}.svc.cluster.local
CLIENT="$TEZ_BIN/tezos-client -E http://tezos-node.{{ .Release.Namespace }}.svc.cluster.local:8732 -d $TEZ_VAR"
${CLIENT} import secret key faucet ${SECRET_KEY} --force

transfers="[]"

echo "Starting balance monitor."
while true
do
    echo "Checking balances ..."
    while IFS= read -r addr; do
        res=$($CLIENT get balance for $addr | sed 's/ ꜩ//' | cut -d. -f1 | tr -d ',')
        if  [ $res -lt $THRESHOLD ]
        then
            echo -e "\t* $addr balance is too low ($res)"
            transfers=$(echo $transfers | $TEZ_VAR/jq '. + [{"destination": "'$addr'", "amount": "'$amount'" }]')
        fi
    done < $input
    echo "- Finished processing wallets, triggering batch transfers if needed"
    if  [ ! "$transfers" = "[]" ]
    then
        echo -e "\t* Transfer required"
        echo $transfers > $TEZ_VAR/transfers.json
        echo
        echo -e "\t---------------------"
        echo -e "\t  Transfering funds  "
        echo -e "\t---------------------"
        $CLIENT multiple transfers from faucet using $TEZ_VAR/transfers.json -G 1520 --burn-cap 500 --fee-cap 200 -q;
        echo -e "\t---------------------"
        echo -e "\t  Transfer finished  "
        echo -e "\t---------------------"
    fi
    transfers="[]"
    echo "Sleeping for $sleep sec before starting again"
    sleep $sleep
    echo
done
