#!/bin/sh
set -ex

start=$1
end=$2
amount=$3
output_folder=$4
echo "[]" > $output_folder/transfers-${1}-${2}.json
# Create them
i=$start
while [ $i -le $end ]
do
    echo $(jq '. += [{"destination": "hang'$i'", "amount": "'$3'" }]' $output_folder/transfers-${1}-${2}.json) > $output_folder/transfers-${1}-${2}.json
    i=`expr $i + 1`
done
cat $output_folder/transfers-${1}-${2}.json
