
secret=$1
pubk=$2
addresses=$3
output=$4

sk=$(jq '[.[] | .["sk"] = .value | del(.value)]' $1)
pk=$(jq '[.[] | .["pubk"] = .value | del(.value)]' $2)
pkh=$(jq '[.[] | .["pkh"] = .value | del(.value)]' $3)

echo
echo $(jq --argjson sk "$sk" --argjson pk "$pk" --argjson pkh "$pkh" -n '($sk + $pk + $pkh | [group_by(.name)[] | add])') > $output
