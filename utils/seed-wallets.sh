#!/bin/sh
set -ex
export TEZOS_CLIENT_UNSAFE_DISABLE_DISCLAIMER=yes

batches_folder="$1"
tezos_node_0="unencrypted:edsk3gUfUPyBSfrS9CCgmCiQsTCHGkviBDusMxDJstFtojtc1zcpsh"
TEZ_BIN=/opt/tezos
TEZ_VAR=/home/florian
CLIENT="$TEZ_BIN/tezos-client -l -E http://tezos-node.prod -d $TEZ_VAR/temp-client2"
${CLIENT} import secret key tezos-node-0 ${tezos_node_0} --force

for batch in $batches_folder/*
do
    echo "$batch"
    $CLIENT multiple transfers from tezos-node-0 using $batch -G 1520 --burn-cap 500 --fee-cap 200;
done
