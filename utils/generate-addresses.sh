#!/bin/sh
set -ex
export TEZOS_CLIENT_UNSAFE_DISABLE_DISCLAIMER=yes

TEZ_BIN=/usr/local/bin
TEZ_VAR=/var/tezos
CLIENT="$TEZ_BIN/tezos-client -d $TEZ_VAR/client"

# Create them
max=20000
i=0
while [ $i -le $max ]
do
    $CLIENT gen keys hang${i} --force;
    i=`expr $i + 1`
done
cat /tmp/transfers.json

# sk=$(jq '[.[] | .["sk"] = .value | del(.value)]' ~/.tezos-client/secret_keys)
# pk=$(jq '[.[] | .["pubk"] = .value | del(.value)]' ~/.tezos-client/public_keys)
# pkh=$(jq '[.[] | .["pkh"] = .value | del(.value)]' ~/.tezos-client/public_key_hashs)

#jq --argjson sk "$sk" --argjson pk "$pk" --argjson pkh "$pkh" -n '($sk + $pk + $pkh | [group_by(.name)[] | add])'
# cp ~/.tezos-client/secret_keys /tmp/secret_keys
# sed 's/unencrypted://g' /tmp/secret_keys > /tmp/keys.json
# cat /tmp/keys.json
