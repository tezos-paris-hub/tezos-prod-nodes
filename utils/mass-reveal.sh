start=$1
end=$2
i=$start

TEZ_VAR=/home/florian/temp-client2
TEZ_BIN=/opt/tezos
CLIENT_DIR="$TEZ_VAR/client"
NODE_DIR="$TEZ_VAR/node"
NODE_DATA_DIR="$TEZ_VAR/node/data"

CLIENT="$TEZ_BIN/tezos-client --endpoint http://tezos-node.prod -w none -d /home/florian/tezos/tezos-infra/load-testing/wallet/raw"


while [ $i -le $end ]
do
    $CLIENT reveal key for hang$i
    i=`expr $i + 1`
done