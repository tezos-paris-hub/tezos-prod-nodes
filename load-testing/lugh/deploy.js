const taquito = require('@taquito/taquito');
const signer = require('@taquito/signer');

const publisher_key = "edsk3gUfUPyBSfrS9CCgmCiQsTCHGkviBDusMxDJstFtojtc1zcpsh";
const fees_faucet = "tz1KqTpEZ7Yob7QbPE4Hy4Wo8fHG8LhKxZSx";
const owner = "tz1KqTpEZ7Yob7QbPE4Hy4Wo8fHG8LhKxZSx";
const admin = "tz1KqTpEZ7Yob7QbPE4Hy4Wo8fHG8LhKxZSx";
const master_minter = "tz1KqTpEZ7Yob7QbPE4Hy4Wo8fHG8LhKxZSx";
const admin_key = "edsk3gUfUPyBSfrS9CCgmCiQsTCHGkviBDusMxDJstFtojtc1zcpsh";

const fees_contract = require('./contracts/fees_manager.json');
const eurl_contract = require('./contracts/eurl.json');
const list_of_addresses = require('../wallet/raw/all_wallets')

const deploy = async resolve => {
    const Tezos = new taquito.TezosToolkit(`http://104.196.8.169:8732/`);
    Tezos.setProvider({ signer: await signer.InMemorySigner.fromSecretKey(publisher_key) });

    // ORIGINATION
    const emptyMap = new taquito.MichelsonMap();

    // Originate Fees manager
    let operation = await Tezos.contract.originate({ code: fees_contract, storage: {
        administrator: admin,
        gasFee: 0,
        gaslessFee: 0,
        storageFee: 140000,
        threshold: 30000,
    }});
    await operation.confirmation();
    console.log(`Fees manager published with ${operation.hash}`);
    const feesManager = await (await operation.contract()).address;
    console.log(`Fees manager address: ${feesManager}`);

    // Originate Eurl FA2 token
    const mintedMap = new taquito.MichelsonMap();
    const metadataMap = new taquito.MichelsonMap();
    const tokenMetadataMap = new taquito.MichelsonMap();
    const tokenInfoMap = new taquito.MichelsonMap();
    mintedMap.set('0','0');
    metadataMap.set('','697066733a2f2f516d6366476468623555747a6d696b50546d51594e64526e586642596b4a443155657a4267616a733138796e554e');
    tokenInfoMap.set('decimals', '36');
    tokenInfoMap.set('is_boolean_amount', '46616c7365');
    tokenInfoMap.set('is_transferable', '54727565');
    tokenInfoMap.set('name', '4c756768204575726f2070656767656420737461626c65636f696e');
    tokenInfoMap.set('should_prefer_symbol', '54727565');
    tokenInfoMap.set('symbol', '4555524c');
    tokenInfoMap.set('thumbnailUri', '697066733a2f2f516d63717359516e3870547851723350316459706759785161364751506d6f425453575138627075464575617165');
    tokenMetadataMap.set('0',{token_id: '0', token_info: tokenInfoMap});
    const ledgerMap = new taquito.MichelsonMap();
    console.log(list_of_addresses.length);
    let index = 200;
    for (const address of list_of_addresses.slice(0, 200)) {
        ledgerMap.set(
            {
              // Pair as Key
              0: address, //nat
              1: '0', //address
            },
            { balance: '1000000000000', lock: false } //1M Eur per account
          );
    }
    operation = await Tezos.contract.originate({ code: eurl_contract, storage: {
        administrator: admin,
        all_tokens: 1,
        default_expiry: 345600,
        fees_faucet: fees_faucet,
        fees_manager: feesManager,
        ledger: ledgerMap,
        master_minted: mintedMap,
        master_minter: master_minter,
        max_expiry: 2678400,
        metadata: metadataMap,
        minters: emptyMap,
        operators: emptyMap,
        owner: owner,
        pause: false,
        permits: emptyMap,
        rights_manager: null,
        token_metadata: tokenMetadataMap,
        total_supply: mintedMap,
        transfer_operation: 0,
    }});
    await operation.confirmation();
    console.log(`EURL contract with ${operation.hash}`);
    const eurl = await (await operation.contract()).address;
    console.log(`EURL address: ${eurl}`);

    // Mint funds
    const contractEurl = await Tezos.contract.at(eurl);
    Tezos.setProvider({ signer: await signer.InMemorySigner.fromSecretKey(admin_key) });
    operation = await contractEurl.methods.mint(admin, (list_of_addresses.length - index) * 1000000000000, 0).send(); // Mint a total Supply i.e. 1M for each address
    await operation.confirmation();
    console.log(`Funds minted with ${operation.hash}`);

    // Batch transfer funds
    while (index < list_of_addresses.length) {
        console.log(index)
        let txs = []
        for (const address of list_of_addresses.slice(index, ((index + 500) < list_of_addresses.length)? index + 500 : list_of_addresses.length)) {
            txs.push({to_: address, token_id: 0, amount: 1000000000000}) // transfer 1 million per address
        }
        operation = await contractEurl.methods.transfer([
            {
                from_: admin,
                txs: txs,
            },
        ]).send();
        await operation.confirmation();
        console.log(`Funds transfered with ${operation.hash}`);
        index = index + 500;
    }
}

deploy().catch(error => { console.log(error)})
